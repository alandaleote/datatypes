﻿using System;

namespace DataTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            byte b;
            b = byte.MaxValue;
            Console.WriteLine("Valor máximo de byte: " + b);
            b = byte.MinValue;
            Console.WriteLine($"Valor mínimo de byte: {b}");
            int i;
            i = int.MaxValue;
            Console.WriteLine("Valor máximo de int: " + i);
            long l;
            l = long.MaxValue;
            Console.WriteLine("Valor máximo de long: " + l);

            string str1 = "Hello";
            string str2 = "World";
            string frase = str1+" "+str2+"!";
            Console.WriteLine(frase);
            int tam = frase.Length;
            string strTamanho = "Tamanho = " + tam.ToString();
            Console.WriteLine(strTamanho);
            Console.WriteLine(frase.Substring(0,5));

            DateTime dt = new DateTime(2018, 04, 23);
            Console.WriteLine(dt.ToString());

            





        }
    }
}
